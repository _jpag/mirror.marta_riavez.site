<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BaseSite
 */

$current_lang = get_bloginfo('language');

?>
<div class="push"></div>
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-footer__inner">
			<div class="footer-col1">
				<img class="footer-logo" src="<?php bloginfo('template_url'); ?>/wp-content/images/menu__logo--white.svg" alt="Marta Riavez">
				<p>
				Marta Riavez - P.IVA 01300050323 - Operazione effettuata in regime fiscale ai sensi dell’art. 1, 
				<br>commi da 54 a 89 della Legge n. 190/2014 – Regime forfetario.                                         
				<br>
				mail: <a href="mailto:info@martariavez.com">info@martariavez.com</a> - cel. <a href="tel+393481234567">+39.349.4367768</a> 
				<br>
				<br>
				<a style="text-decoration:underline;" target="_blank" href="<?php if ($current_lang === 'it-IT') {
							echo '/it/termini-e-condizioni';
						} else if ($current_lang === 'en-US') {
							echo '/en/terms-and-conditions';
						} ?>"><?php esc_html_e( 'Terms and conditions', 'basesite' ); ?> <?php $current_lang ?>
				</a>
				<br>
				<br>
				<a style="text-decoration:underline;" target="_blank" href="https://www.iubenda.com/privacy-policy/51864988" class="iubenda-white iubenda-embed " title="Privacy Policy">Privacy Policy</a> <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>				
				<a style="text-decoration:underline;" target="_blank" href="https://www.iubenda.com/privacy-policy/51864988/cookie-policy" class="iubenda-white iubenda-embed " title="Cookie Policy">Cookie Policy</a> <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
			</p>
			</div>	
			<a href="http://www.lucapagot.it" target="_blank" class="footer-col2">
				<p>powered by </p><img class="luca-pagot" src="<?php bloginfo('template_url'); ?>/wp-content/images/web-design-luca-pagot.svg" alt="Pagot Luca Web Design">
			</a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
