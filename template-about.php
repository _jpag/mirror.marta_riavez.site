<?php /* Template Name: BaseSite About  */ get_header(); ?>

<!-- site content -->
<div id="content" class="site-content">

	<div id="primary" class="content-area">
		<main id="main" class="site-main c-about">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'about' );

				// If comments are open or we have at least one comment, load up the comment template.
				//if ( comments_open() || get_comments_number() ) :
				//	comments_template();
				//endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

	</div>
<?php
//get_sidebar();
get_footer();