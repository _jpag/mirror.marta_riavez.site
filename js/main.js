var isMobile = function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
    } else {
        return false;
    }
}

/*----- FUNCTIONS -----*/
var fadeLoad = function() {

};

var bodyScroll = function () {
    if (isMobile() === false) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('body').addClass('isScroll');
            } else {
                $('body').removeClass('isScroll');
            }
        });
    }
};

var navMobile = function () {
    var b = $('.menu-toggle');
    b.on('click', function () {
        $(this).toggleClass('isOpen');
        $('body').toggleClass('isOpen');
    });
};

var resetNavHeight = function() {
    var b = $('.menu-toggle');
    var w = $(window).innerWidth();
    if (w <= 1024 && !isMobile() ) {
        b.removeClass('isOpen');
        $('body').removeClass('isOpen');
    }
};

var slideshow = function () {
    if ($('.c-hero-slideshow').length > 0) {
        $('.c-hero-slideshow').cycle({
            speed: 600,
            manualSpeed: 400,
            timeout: 6000,
            slides: '> .item',
            pager: '> .c-slideshow-pager',
            pagerTemplate: '<div class="c-slideshow-pager__item"></div>',
            pagerActiveClass: 'isActive',
            log: false,
            swipe: true,
        });
    }

    if ($('.about--slideshow').length > 0) {
        $('.about--slideshow').cycle({
            speed: 600,
            manualSpeed: 400,
            timeout: 15000,
            slides: '> .item',
            pager: '> .c-slideshow-pager',
            pagerTemplate: '<div class="c-slideshow-pager__item"></div>',
            pagerActiveClass: 'isActive',
            log: false,
            swipe: true,
            autoHeight: 'calc',
            paused: true,
            prev: "#prev",
            next: "#next"
        });
    }
};

var descriptionTab = function() {
    var des = $('.description_tab');
    var inf = $('.additional_information_tab');
    if  (!inf.length > 0) {
        des.remove();        
    }
};
descriptionTab();

$(document).ready(function() {
    navMobile();
    bodyScroll();
    slideshow();
});

$(window).resize(function() {
    resetNavHeight();
});