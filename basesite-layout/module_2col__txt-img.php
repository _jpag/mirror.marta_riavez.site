
<!-- start / ACF layout module_2col__txt-img -->
  <div class="l-container l-row l-margin--top l-margin--bottom l-bg__gradient l-table--parent u-clearfix">
   <div class="l-cln__2 l-table--children l-padding">
     <h2 class="txt--s__xl txt--col__2 l-title--decoration l-title--decoration--white txt--weight__bold"><?php the_sub_field('module_2col__txt-img_title'); ?></h2>
     <p class="txt--s__m txt--col__2"><?php the_sub_field('module_2col__txt-img_text'); ?></p>
   </div>
   <div class="l-cln__2 l-h__100 l-bg__cover l-row l-bg__frame l-bg__frame--right" style="background-image: url('<?php the_sub_field('module_2col__txt-img_image'); ?>')"></div>
  </div>
  <!-- end / ACF module_2col__txt-img -->
