<!-- start / ACF layout module_1col__about-slider -->
  <div class="module_1col__about-slider_video">
    <div>
      <video width="" height="" controls muted>
        <source src="<?php the_sub_field('video'); ?>" type="video/mp4">
      </video>
      <div class="about--slideshow">
        <?php	while(has_sub_field('item') ): ?>   
          <div class="item">
            <h1 class="module_1col__about-slider--title about--title"><?php the_sub_field('title'); ?></h1>
            <p class="module_1col__about-slider--text about--text"><?php the_sub_field('text'); ?></p>
          </div>
        <?php endwhile; ?>
        <div class="c-slideshow-pager"></div>
      <!-- slider buttons -->
      <div class="c-btnslider">
        <span id="prev" class="c-btn c-btn--left"></span>
        <span id="next" class="c-btn c-btn--right"></span>
      </div>
      </div>
    </div>  
 </div>
 <!-- end / ACF module_1col__about-slider -->
