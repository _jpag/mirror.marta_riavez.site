<!-- start / ACF layout module_2col__txt-slider -->
  <div class="module_2col__txt-slider l-container l-row l-margin--top l-margin--bottom l-bg__gradient l-table--parent u-clearfix">
   <div class="l-cln__2 l-table--children l-padding">
     <h2 class="txt--s__xl txt--col__2 l-title--decoration l-title--decoration--white txt--weight__bold"><?php the_sub_field('module_2col__txt_title'); ?></h2>
     <p class="txt--s__m txt--col__2"><?php the_sub_field('module_2col__txt_text'); ?></p>
   </div>
   <div class="module_2col__txt-slider--container l-cln__2 l-h__100 l-h__50 l-bg__frame l-bg__frame--right">
   <?php	while(has_sub_field('2col_item') ): ?>
     <div class="item l-bg__cover l-cln__1 l-row l-h__100 l-h__50" style="background-image: url('<?php the_sub_field('module_2col__slider_image'); ?>')"></div>
   <?php endwhile; ?>
  </div>
  <div class="c-slideshow-pager"></div>
 </div>
 <!-- end / ACF module_2col__txt-slider -->
