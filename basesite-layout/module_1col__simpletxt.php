<!-- start / ACF layout module_1col__simpletxt -->
<div class="module_1col__simpletxt">
   <h1 class="module_1col__simpletxt--title"><?php the_sub_field('module_1col__simpletxt--title_title'); ?></h1>
   <p class="module_1col__simpletxt--text"><?php the_sub_field('module_1col__txt--simpletext_text'); ?></p>

   <?php	if( have_rows('button_wp') ): ?>
        <?php while( have_rows('button_wp') ) : the_row(); ?>
            <a class="o-btn o-btn--right" href="<?php the_sub_field('row_wp_link_wp'); ?>"><?php the_sub_field('row_wp_title'); ?></a>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php	if( have_rows('button_url') ): ?>
        <?php while( have_rows('button_url') ) : the_row(); ?>
            <a class="o-btn o-btn--right" href="<?php the_sub_field('row_url_link_url'); ?>" target="_blank"><?php the_sub_field('row_url_title'); ?></a>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!-- end / ACF layout module_1col__simpletxt -->
