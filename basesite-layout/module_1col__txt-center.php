<!-- start / ACF layout module_1col__txt-center-->
<div class="l-margin--top l-margin--bottom">
 <div class="l-container_s u-clearfix module_1col__txt-center ">
  <div class="module_1col__txt-center__icon <?php the_sub_field('icon'); ?>"></div>
   <h1 class="txt--s__xl txt--col__3 txt--weight__bold txt__center"><?php the_sub_field('module_1col__txt-center--title_title'); ?></h1>
   <?php the_sub_field('textarea'); ?>

   <?php	if( have_rows('button_wp') ): ?>
     <?php while( have_rows('button_wp') ) : the_row(); ?>
       <a class="o-btn o-btn--dark-right o-btn--right" href="<?php the_sub_field('row_wp_link_wp'); ?>"><?php the_sub_field('row_wp_title'); ?></a>
     <?php endwhile; ?>
   <?php endif; ?>

   <?php	if( have_rows('button_url') ): ?>
     <?php while( have_rows('button_url') ) : the_row(); ?>
       <a class="o-btn o-btn--dark-right o-btn--right" href="<?php the_sub_field('row_url_link_url'); ?>" target="_blank"><?php the_sub_field('row_url_title'); ?></a>
     <?php endwhile; ?>
   <?php endif; ?>


 </div>
</div>
<!-- end / ACF layout module_1col__txt-center-->
