<!-- start / ACF layout module_3col__txt -->
<div class=" module_3col__txt l-row l-margin--top l-bottom__gradient">
   <div class="l-container l-table--parent l-padding-b__16">
    <h2 class="txt--s__xl txt--col__3 txt--weight__bold"><?php the_sub_field('3col__title_title'); ?></h2>

   <?php while ( has_sub_field('3col__item') ) : ?>
     <div class="l-cln__3 c-3col__item l-table--children l-padding-r__48">
       <div class="c-3col__item--icon <?php the_sub_field('3col__icon_icon'); ?>"></div>
       <h2 class="txt--s__l txt--col__4 txt--weight__bold"><?php the_sub_field('3col__subtitle_title'); ?></h2>
       <p class="txt--s__m txt--col__4"><?php the_sub_field('3col__text_text'); ?></p>
     </div>
   <?php endwhile; ?>

  </div>
</div>
<!-- end / ACF layout module_3col__txt -->
