<!-- start / ACF layout module_3col__gallery -->
<div class="module_3col__gallery l-margin--top l-bottom__grey-gradient l-padding-b__8">
 <div class="l-container u-clearfix">
   <h1 class="txt--s__xl txt--col__4 l-title--decoration l-title--decoration--grey txt--weight__bold"><?php the_sub_field('module_3col__gallery--title_title'); ?></h1>
   <br>
   <?php
   $images = get_sub_field('gallery');
   if( $images ): ?>
       <div class="gallery_inner">
           <?php foreach( $images as $image ): ?>
               <span class="gallery_item">
                   <a class="fancybox"  rel="group" href="<?php echo $image['url']; ?>">
                        <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                   </a>
                   <p><?php echo $image['caption']; ?></p>
               </span>
           <?php endforeach; ?>
       </div>
   <?php endif; ?>
 </div>
</div>
<!-- end / ACF layout module_3col__gallery -->
