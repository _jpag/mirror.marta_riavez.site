
<!-- start / ACF layout module_1col__img -->
 <div class="module_1col__overlay module_1col__img--image l-bg__cover l-cln__1 l-row l-float-none" style="background-image: url('<?php the_sub_field('module_1col__img_image'); ?>')">
	 <div class="module_1col__img--content"></div>
	 <div class="module_1col__text l-bg__color l-padding">
		<div>
			<h2 class="txt--s__l txt--col__2 txt--weight__bold"><?php the_sub_field('overlay_text_title'); ?></h2>
			<p class="txt--s__m txt--col__2"><?php the_sub_field('overlay_text_text'); ?></p>

      <?php	if( have_rows('button_wp') ): ?>
        <?php while( have_rows('button_wp') ) : the_row(); ?>
          <a class="o-btn o-btn--right" href="<?php the_sub_field('row_wp_link_wp'); ?>"><?php the_sub_field('row_wp_title'); ?></a>
        <?php endwhile; ?>
      <?php endif; ?>

      <?php	if( have_rows('button_url') ): ?>
        <?php while( have_rows('button_url') ) : the_row(); ?>
          <a class="o-btn o-btn--right" href="<?php the_sub_field('row_url_link_url'); ?>" target="_blank"><?php the_sub_field('row_url_title'); ?></a>
        <?php endwhile; ?>
      <?php endif; ?>

		</div>
	 </div>
</div>
<!-- end / ACF module_1col__img -->
