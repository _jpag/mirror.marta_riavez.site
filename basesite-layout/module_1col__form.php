<!-- start / ACF layout Form -->
<div class="module_1col__form l-container_s l-row l-margin--top l-margin--bottom">
  <h2 class="txt--s__xl txt--col__4 l-title--decoration l-title--decoration--grey txt--weight__bold"><?php the_sub_field('title'); ?></h2>
 <?php the_sub_field('form_shortcode'); ?>
 <?php the_sub_field('contact_form'); ?>

</div>
<!-- end / ACF Form -->
