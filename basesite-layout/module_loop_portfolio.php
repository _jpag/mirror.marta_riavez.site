<?php $post_objects = get_sub_field('post_portfolio');

if( $post_objects ): ?>
    <ul>
    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <li>
          <!-- post thumbnail -->
        	<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
        		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        			<?php the_post_thumbnail(array(120,120)); // Declare pixel size you need inside the array ?>
        		</a>
        	<?php endif; ?>
        	<!-- /post thumbnail -->

        	<!-- post title -->
        	<h2>
        		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
        	</h2>
        	<!-- /post title -->
        </li>
    <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif;
