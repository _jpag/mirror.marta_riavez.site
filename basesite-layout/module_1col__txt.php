<!-- start / ACF layout module_1col__txt -->
<div class="module_1col__txt">
   <h1 class="module_1col__txt--title"><?php the_sub_field('module_1col__txt--title_title'); ?></h1>
   <p class="module_1col__txt--text"><?php the_sub_field('textarea'); ?></p>
</div>
<!-- end / ACF layout module_1col__txt -->
