<!-- start / ACF layout module_1col__about-slider -->
  <div class="module_1col__about-slider">
    <div class="about--slideshow">
      <?php	while(has_sub_field('item') ): ?>   
        <div class="item">
          <img class="about--img" src="<?php the_sub_field('image'); ?>" alt="Marta Riavez">
          <h1 class="module_1col__about-slider--title about--title"><?php the_sub_field('title'); ?></h1>
          <p class="module_1col__about-slider--text about--text"><?php the_sub_field('text'); ?></p>
        </div>
      <?php endwhile; ?>
      <div class="c-slideshow-pager"></div>
		<!-- slider buttons -->
		<div class="c-btnslider">
			<span id="prev" class="c-btn c-btn--left"></span>
			<span id="next" class="c-btn c-btn--right"></span>
		</div>
    </div>

 </div>
 <!-- end / ACF module_1col__about-slider -->
