<!-- start / ACF layout module_1col__map -->
<div class="module_1col__map l-row l-margin--top">
 <div class="l-container">
     <div class="module_1col__map--content">
       <div class="module_1col__map--text l-padding">
         <div>
           <p class="txt--s__l txt--weight__bold txt--col__2 txt--weight__bold"><?php the_sub_field('title'); ?></p>
           <p class="txt--s__m"><?php the_sub_field('textarea'); ?></p>
           <span class="btn__show_hide"></span>

         </div>
       </div>
     </div>
 </div>
  <?php
  $location = get_sub_field('module_1col__map--item');

  if( !empty($location) ):
  ?>
  <div class="acf-map">
   <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
  </div>
  <?php endif; ?>
 </div>
 <!-- end / ACF module_1col__map-->
