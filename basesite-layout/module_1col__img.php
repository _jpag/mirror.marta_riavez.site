<!-- start / ACF layout module_1col__img -->
  <div class="module_1col__img module_1col__img--image l-bg__cover l-cln__1 l-row l-float-none" style="background-image: url('<?php the_sub_field('module_1col__img_image'); ?>')">
    <div class="module_1col__img--content">
      <h2 class="txt--s__xl txt--col__2 l-title--decoration l-title--decoration--gradient txt--weight__bold"><?php the_sub_field('module_1col__img_title'); ?></h2>

      <?php	if( have_rows('module_1col__img_btn_button_wp') ): ?>
       <?php while( have_rows('module_1col__img_btn_button_wp') ) : the_row(); ?>
         <a class="o-btn o-btn--right c-hero-slideshow__btn" href="<?php the_sub_field('row_wp_link_wp'); ?>"><?php the_sub_field('row_wp_title'); ?></a>
       <?php endwhile; ?>
     <?php endif; ?>

     <?php	if( have_rows('module_1col__img_btn_button_url') ): ?>
       <?php while( have_rows('module_1col__img_btn_button_url') ) : the_row(); ?>
         <a class="o-btn o-btn--right c-hero-slideshow__btn" href="<?php the_sub_field('row_url_link_url'); ?>" target="_blank"><?php the_sub_field('row_url_title'); ?></a>
       <?php endwhile; ?>
     <?php endif; ?>
   </div>
   <div class="c-hero__gradient"></div>
 </div>
 <!-- end / ACF module_1col__img -->
