<!-- start / ACF layout module_1col__about -->
<div class="module_1col__about">
    <div>
        <img class="about--img" src="<?php the_sub_field('image'); ?>" alt="Marta Riavez">
        <h1 class="module_1col__about--title about--title"><?php the_sub_field('module_1col__about--title_title'); ?></h1>
        <p class="module_1col__about--text about--text"><?php the_sub_field('textarea'); ?></p>
    </div>
</div>
<!-- end / ACF layout module_1col__about -->
