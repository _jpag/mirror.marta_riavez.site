<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BaseSite
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'basesite' ); ?></a>
		<div class="site-branding">
			<?php the_custom_logo(); ?>
		</div><!-- .site-branding -->
	<header id="masthead" class="site-header">

	<a href="javascript:;" class="menu-toggle">
		<span class="line line--1"></span>
		<span class="line line--2"></span>
		<span class="line line--3"></span>
		<span class="line line--4"></span>
	</a>

		<nav id="site-navigation" class="main-navigation">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'left-menu',
				) );
				wp_nav_menu( array(
					'theme_location' => 'menu-2',
					'menu_id'        => 'right-menu',
				) );
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->