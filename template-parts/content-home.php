<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BaseSite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'basesite' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<!-- basesite layout -->
	<?php
	if( get_field('page_layout') ):
		while ( has_sub_field('page_layout') ) : ?>
				<?php if( get_row_layout() == 'module_1col__form' ):
						get_template_part('basesite-layout/module_1col__form');
					endif; ?>
				<?php if( get_row_layout() == 'module_1col__txt' ):
						get_template_part('basesite-layout/module_1col__txt');
					endif; ?>

				<?php if( get_row_layout() == 'module_1col__simpletxt' ):
						get_template_part('basesite-layout/module_1col__simpletxt');
					endif; ?>

				<?php if( get_row_layout() == 'module_1col__txt-center' ):
						get_template_part('basesite-layout/module_1col__txt-center');
					endif; ?>

				<?php if( get_row_layout() == 'module_2col__txt-slider' ):
						get_template_part('basesite-layout/module_2col__txt-slider');
					endif; ?>

				<?php if( get_row_layout() == 'woocommerce_category_link' ):
						get_template_part('basesite-layout/module_2col__cat-woocommerce');
					endif; ?>

				<?php if( get_row_layout() == 'module_1col__title' ):
						get_template_part('basesite-layout/module_1col__title');
					endif; ?>

		<?php endwhile;
	else : endif;
	?>
	<!-- /basesite layout -->

	<!-- woocommerce categories -->
	<div class="container-categories">
		<div class="section-title">
			<h2><?php esc_html_e( 'Products', 'basesite' ); ?></h2>
		</div>
		<div class="c-home_categories">
			<?php 
			$prod_categories = get_terms( 'product_cat', array(
				'orderby'    => 'name',
				'order'      => 'ASC',
				'hide_empty' => true
			));
			foreach( $prod_categories as $prod_cat ) :
				$cat_thumb_id = get_woocommerce_term_meta( $prod_cat->term_id, 'thumbnail_id', true );
				$shop_catalog_img = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
				$term_link = get_term_link( $prod_cat, 'product_cat' );?>
			<div class="c-home_categories__item">
				<h2 class="c-home_categories__title"><a href="<?php echo $term_link; ?>"><?php echo $prod_cat->name; ?></a></h2>
				<a class="o-img-hover c-home_categories__img" href="<?php echo $term_link; ?>"><img src="<?php echo $shop_catalog_img[0]; ?>" alt="<?php echo $prod_cat->name; ?>" /></a>
				<a class="o-btn o-btn--right c-home_categories__btn" href="<?php echo $term_link; ?>"><?php esc_html_e( 'view catalog', 'basesite' ); ?></a>
			</div>	
			<?php endforeach; wp_reset_query();
			?>
		</div>
	</div>
	<!-- /woocommerce categories -->


	<!-- woocommerce featured products -->
	<div class="container-products">
		<div class="section-title">
			<h2>Best seller</h2>
		</div>
		<ul class="products">
			<?php
				$args = array(
					'post_type' => 'product',
					'posts_per_page' => 3,
					'tax_query' => array(
							array(
								'taxonomy' => 'product_visibility',
								'field'    => 'name',
								'terms'    => 'featured',
							),
						),
					);
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) : $loop->the_post();
						wc_get_template_part( 'content', 'product_home' );
					endwhile;
				} else {
					echo __( 'No products found' );
				}

				wp_reset_postdata();
			?>
		</ul>
	</div>
	<!-- /woocommerce featured products -->


</article><!-- #post-<?php the_ID(); ?> -->
