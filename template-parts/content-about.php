<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BaseSite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'basesite' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<!-- basesite layout -->
	<?php
	if( get_field('page_layout') ):
		while ( has_sub_field('page_layout') ) : ?>
				<?php if( get_row_layout() == 'module_1col__form' ):
						get_template_part('basesite-layout/module_1col__form');
					endif; ?>
				<?php if( get_row_layout() == 'module_1col__txt' ):
						get_template_part('basesite-layout/module_1col__txt');
					endif; ?>

				<?php if( get_row_layout() == 'module_1col__simpletxt' ):
						get_template_part('basesite-layout/module_1col__simpletxt');
					endif; ?>

				<?php if( get_row_layout() == 'module_1col__txt-center' ):
						get_template_part('basesite-layout/module_1col__txt-center');
					endif; ?>

				<?php if( get_row_layout() == 'module_2col__txt-slider' ):
						get_template_part('basesite-layout/module_2col__txt-slider');
					endif; ?>

				<?php if( get_row_layout() == 'woocommerce_category_link' ):
						get_template_part('basesite-layout/module_2col__cat-woocommerce');
					endif; ?>

				<?php if( get_row_layout() == 'module_1col__about' ):
						get_template_part('basesite-layout/module_1col__about');
					endif; ?>

				<?php if( get_row_layout() == 'module_1col__about-sliderVideo' ):
						get_template_part('basesite-layout/module_1col__about-slider_video');
					endif; ?>
		<?php endwhile;
	else : endif;
	?>
	<!-- /basesite layout -->

</article><!-- #post-<?php the_ID(); ?> -->
