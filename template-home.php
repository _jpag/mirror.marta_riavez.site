<?php /* Template Name: BaseSite Home  */ get_header(); ?>


<?php if(get_field('hp_hero_slideshow')): ?>
	<!-- start / ACF hero slideshow -->
	<div class="c-hero-slideshow">

			<?php while(has_sub_field('hp_hero_slideshow')): ?>
				<div class="item c-hero-slideshow__image" style="background-image: url('<?php the_sub_field('image'); ?>')">
					<div class="c-hero-slideshow__title">
						<h1 class="txt--s__xl txt--weight__bold"><?php the_sub_field('title'); ?></h1>

						<?php if( have_rows('button_wp') ): ?>
							<?php while( have_rows('button_wp') ) : the_row(); ?>
								<a class="o-btn o-btn--right--white c-hero-slideshow__btn" href="<?php the_sub_field('row_wp_link_wp'); ?>"><?php the_sub_field('row_wp_title'); ?></a>
							<?php endwhile; ?>
						<?php endif; ?>

						<?php if( have_rows('button_url') ): ?>
							<?php while( have_rows('button_url') ) : the_row(); ?>
								<a class="o-btn o-btn--right--white c-hero-slideshow__btn" href="<?php the_sub_field('row_url_link_url'); ?>" target="_blank" ><?php the_sub_field('row_url_title'); ?></a>
							<?php endwhile; ?>
						<?php endif; ?>


						<?php if( have_rows('button_custom') ): ?>
							<?php while( have_rows('button_custom') ) : the_row(); ?>
								<a class="o-btn o-btn--right--white c-hero-slideshow__btn" href="<?php the_sub_field('custom_url'); ?>"><?php the_sub_field('row_url_title'); ?></a>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile; ?>

		<div class="c-slideshow-pager"></div>
	</div>
<!-- end / ACF hero slideshow -->
<?php endif; ?>

<!-- site content -->
<div id="content" class="site-content">

<div id="primary" class="content-area">
	<main id="main" class="site-main c-home">
		
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'home' );

			// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
			//	comments_template();
			//endif;

		endwhile; // End of the loop.
		?>


	</main><!-- #main -->
</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();